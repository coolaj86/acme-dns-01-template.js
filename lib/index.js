'use strict';

var request;
var defaults = {
	wait: 15 * 1000
};

module.exports.create = function(config) {
	var wait = config.wait || defaults.wait;

	return {
		init: function(opts) {
			request = opts.request;
			return null;
		},
		zones: function(data) {
			//console.info('List Zones', data);
			throw Error('listing zones not implemented');
		},
		set: function(data) {
			// console.info('Add TXT', data);
			var ch = data.challenge;
			if (!ch.dnsZone) {
				// zones() is not implemented for http-01 challenges,
				// but it is almost always implemented for dns-01 challenges.
				// Any plugin that implements zones() should expect ch.dnsZone here
				throw new Error('No matching zone for ' + ch.dnsHost);
			}

			throw Error('setting TXT not implemented');
			// many APIs return a valid response quite a bit
			// before they actually set the record, and checking
			// too quickly will self-poison the response cache.
			return new Promise(function(resolve) {
				setTimeout(resolve, wait, null);
			});
		},
		remove: function(data) {
			// console.info('Remove TXT', data);
			throw Error('removing TXT not implemented');
		},
		get: function(data) {
			// console.info('List TXT', data);
			throw Error('listing TXTs not implemented');
		}
	};
};
